import json

import pytest
import allure

# url = 'https://api.hh.ru/vacancies'

@allure.feature('All vacancies list')
@allure.story('Получение все вакансий с hh.ru')
def test_get_random_dog(hh_api):
    response = hh_api.get("vacancies")

    with allure.step("Запрос отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

@allure.feature('Params search')
@allure.story('Получение все вакансий с hh.ru в собственнии с заданным запросом')
def test_get_search_results(hh_api):
    params = {
        "text": "qa automation"
    }
    response = hh_api.get("vacancies", params)
    with allure.step("Запрос должен содержать корректную позицию для поиска вакансий."):
        data = json.loads(response.text)
        assert "qa+automation" in data["alternate_url"], f"Поиск не содержит требуемого параметра {params}"
        print(data["alternate_url"])
